package utils;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

/**
 * This class contains all common methods which can be used across all the test classes
 */
public class CommonMethods {
    /**
     * To fetch the response from the endpoint by doing a getRequest
     *
     * @param url is the Endpoint URL to which the request is sent
     * @return response of the request
     */
    public static Response getRequestToEndpointUrlToFetchResponse(String url) {
        RestAssured.defaultParser = Parser.JSON;

        return given().headers("Content-Type", ContentType.JSON).
                when().get(url).
                then().contentType(ContentType.JSON).extract().response();
    }
}

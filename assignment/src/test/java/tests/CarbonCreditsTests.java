package tests;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import org.junit.Test;
import utils.CommonMethods;

import java.util.List;


/**
 * All tests related to carbon credits api are maintained here
 */
public class CarbonCreditsTests extends CommonMethods {
    private static String SERVICE_ENDPOINT_URL = "https://api.tmsandbox.co.nz/v1/Categories/6327/Details.json?catalogue=false";

    @Test
    public void validateElementsOfCarbonCreditsApi() {
        // Logs the response in case of the failures
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        // Get response by sending request to endpoint
        Response response = getRequestToEndpointUrlToFetchResponse(SERVICE_ENDPOINT_URL);

        // Asserting to verify the successful response
        response.then().assertThat().statusCode(200);

        // Get the JsonPath to do further validations
        JsonPath jsonPathEval = response.jsonPath();

        // Validation - Name as Carbon Credits
        assertEquals("Carbon credits", jsonPathEval.get("Name"));

        // Validation - CanRelist as true
        assertTrue(jsonPathEval.getBoolean("CanRelist"));

        // Validation - Gallery promotion description us validated of Gallery Promotion
        List<String> names = jsonPathEval.getList("Promotions.Name");

        String galleryDescription = null;
        for (int i = 0; i < names.size(); i++) {
            if ("Gallery".equals(names.get(i))) {
                galleryDescription = jsonPathEval.getList("Promotions.Description").get(i).toString();
                break;
            }
        }
        assertThat("Gallery description should contain the expected description", galleryDescription, containsString("2x larger image"));
    }
}

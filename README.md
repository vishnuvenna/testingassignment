# README #

### What is this repository for? ###

* This Repository is to do few test validations on the Cardbon Credits API using Rest Assured with JAVA as a programming language using Maven as the build tool
* The Current framework can be improvised and customised depending on further requirements and test environments

### How do I get set up? ###
* To run the Tests need to follow below 
* Configure Java and Maven
* Open command prompt
* Navigate to the project folder which is '../testingassignment/assignment/' after cloning the current git repository
* Execute 'mvn compile' to download all the project dependencies
* Run test by executing 'mvn test' command which gives the results of the test
* All the tests are placed under '..\testingassignment\assignment\src\test\java\tests\'. So the implementation can be viewed by opening these files.
